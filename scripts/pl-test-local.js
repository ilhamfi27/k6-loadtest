import http from 'k6/http';
import { check, sleep } from 'k6';

export let options = {
  stages: [
    { duration: '2s', target: 100 },
    { duration: '2s', target: 100 },
    { duration: '2s', target: 200 },
    { duration: '2s', target: 200 },
    { duration: '2s', target: 300 },
    { duration: '2s', target: 300 },
    { duration: '2s', target: 400 },
    { duration: '2s', target: 400 },
    { duration: '2s', target: 500 },
    { duration: '2s', target: 500 },
    { duration: '2s', target: 600 },
    { duration: '2s', target: 600 },
    { duration: '2s', target: 700 },
    { duration: '2s', target: 700 },
    { duration: '2s', target: 800 },
    { duration: '2s', target: 800 },
    { duration: '2s', target: 900 },
    { duration: '2s', target: 900 },
    { duration: '2s', target: 1000 },
    { duration: '2s', target: 1000 },

    { duration: '2s', target: 1100 },
    { duration: '2s', target: 1100 },
    { duration: '2s', target: 1200 },
    { duration: '2s', target: 1200 },
    { duration: '2s', target: 1300 },
    { duration: '2s', target: 1300 },
    { duration: '2s', target: 1400 },
    { duration: '2s', target: 1400 },
    { duration: '2s', target: 1500 },
    { duration: '2s', target: 1500 },
    { duration: '2s', target: 1600 },
    { duration: '2s', target: 1600 },
    { duration: '2s', target: 1700 },
    { duration: '2s', target: 1700 },
    { duration: '2s', target: 1800 },
    { duration: '2s', target: 1800 },
    { duration: '2s', target: 1900 },
    { duration: '2s', target: 1900 },
    { duration: '2s', target: 2000 },
    { duration: '2s', target: 2000 },

    { duration: '2s', target: 2100 },
    { duration: '2s', target: 2100 },
    { duration: '2s', target: 2200 },
    { duration: '2s', target: 2200 },
    { duration: '2s', target: 2300 },
    { duration: '2s', target: 2300 },
    { duration: '2s', target: 2400 },
    { duration: '2s', target: 2400 },
    { duration: '2s', target: 2500 },
    { duration: '2s', target: 2500 },
    { duration: '2s', target: 2600 },
    { duration: '2s', target: 2600 },
    { duration: '2s', target: 2700 },
    { duration: '2s', target: 2700 },
    { duration: '2s', target: 2800 },
    { duration: '2s', target: 2800 },
    { duration: '2s', target: 2900 },
    { duration: '2s', target: 2900 },
    { duration: '2s', target: 3000 },
    { duration: '2s', target: 3000 },

    { duration: '2s', target: 3100 },
    { duration: '2s', target: 3100 },
    { duration: '2s', target: 3200 },
    { duration: '2s', target: 3200 },
    { duration: '2s', target: 3300 },
    { duration: '2s', target: 3300 },
    { duration: '2s', target: 3400 },
    { duration: '2s', target: 3400 },
    { duration: '2s', target: 3500 },
    { duration: '2s', target: 3500 },
    { duration: '2s', target: 3600 },
    { duration: '2s', target: 3600 },
    { duration: '2s', target: 3700 },
    { duration: '2s', target: 3700 },
    { duration: '2s', target: 3800 },
    { duration: '2s', target: 3800 },
    { duration: '2s', target: 3900 },
    { duration: '2s', target: 3900 },
    { duration: '2s', target: 4000 },
    { duration: '2s', target: 4000 },

    { duration: '2s', target: 4100 },
    { duration: '2s', target: 4100 },
    { duration: '2s', target: 4200 },
    { duration: '2s', target: 4200 },
    { duration: '2s', target: 4300 },
    { duration: '2s', target: 4300 },
    { duration: '2s', target: 4400 },
    { duration: '2s', target: 4400 },
    { duration: '2s', target: 4500 },
    { duration: '2s', target: 4500 },
    { duration: '2s', target: 4600 },
    { duration: '2s', target: 4600 },
    { duration: '2s', target: 4700 },
    { duration: '2s', target: 4700 },
    { duration: '2s', target: 4800 },
    { duration: '2s', target: 4800 },
    { duration: '2s', target: 4900 },
    { duration: '2s', target: 4900 },
    { duration: '2s', target: 5000 },
    { duration: '2s', target: 5000 },

    { duration: '120s', target: 5000 },
    { duration: '2s', target: 5000 },

    { duration: '2s', target: 0 },
  ],
};

export default function () {
  const url = 'http://103.31.39.10:3000/passport/v1/nik/scan?nik=1234567890123456';
  const params = {
    headers: {
      Accepts: 'application/json',
    },
  };
  const response = http.get(url, params);
  check(response, {
    'status is 200': (r) => r.status === 200,
    'status is not 200': (r) => {
      if (r.status !== 200) {
        console.log(r);
      }
      return r.status !== 200;
    },
  });
  sleep(0.3);
}
