import http from 'k6/http';
import { check, sleep } from 'k6';

export let options = {
  stages: [
    { duration: '2s', target: 100 },
    { duration: '2s', target: 100 },
    { duration: '2s', target: 200 },
    { duration: '2s', target: 200 },
    { duration: '2s', target: 300 },
    { duration: '2s', target: 300 },
    { duration: '2s', target: 400 },
    { duration: '2s', target: 400 },
    { duration: '2s', target: 500 },
    { duration: '2s', target: 500 },
    { duration: '2s', target: 600 },
    { duration: '2s', target: 600 },
    { duration: '2s', target: 700 },
    { duration: '2s', target: 700 },
    { duration: '2s', target: 800 },
    { duration: '2s', target: 800 },
    { duration: '2s', target: 900 },
    { duration: '2s', target: 900 },
    { duration: '2s', target: 1000 },
    { duration: '2s', target: 1000 },

    { duration: '2s', target: 1100 },
    { duration: '2s', target: 1100 },
    { duration: '2s', target: 1200 },
    { duration: '2s', target: 1200 },
    { duration: '2s', target: 1300 },
    { duration: '2s', target: 1300 },
    { duration: '2s', target: 1400 },
    { duration: '2s', target: 1400 },
    { duration: '2s', target: 1500 },
    { duration: '2s', target: 1500 },
    { duration: '2s', target: 1600 },
    { duration: '2s', target: 1600 },
    { duration: '2s', target: 1700 },
    { duration: '2s', target: 1700 },
    { duration: '2s', target: 1800 },
    { duration: '2s', target: 1800 },
    { duration: '2s', target: 1900 },
    { duration: '2s', target: 1900 },
    { duration: '2s', target: 2000 },
    { duration: '2s', target: 2000 },

    { duration: '60s', target: 2000 },
    { duration: '2s', target: 2000 },
    { duration: '2s', target: 0 },
  ],
};

export default function () {
  const url = 'https://api.staging.apipedia.co.id/pl-check/0.0.2/check';
  const payload = JSON.stringify({
    nik: '1234567890123456',
    nama: 'Raden Ilham Fadhilah Ibadurrohan',
    email: 'ilhamfi_2701@yahoo.com',
    no_telp: '082215561862',
    nomor_perjalanan: 'WIK151',
    asal: 'Bandung',
    tujuan: 'Jakarta',
    waktu_keberangkatan: '2021-07-02 01:00:00',
    waktu_kedatangan: '2021-07-02 01:00:00',
  });
  const params = {
    headers: {
      Accepts: 'application/json',
      'x-api-key': 'CH13m3UgoGQr3C1wmB1xeGJ0hYS1gE6P',
    },
  };
  const response = http.post(url, payload, params);
  check(response, {
    'status is 200': (r) => r.status === 200,
    'status is not 200': (r) => {
      if (r.status !== 200) {
        console.log(r);
      }
      return r.status !== 200;
    },
  });
  sleep(0.3);
}
